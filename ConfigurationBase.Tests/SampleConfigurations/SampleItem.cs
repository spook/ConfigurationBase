﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationBase.Tests.SampleConfigurations
{
    public class SampleItem : BaseCollectionItem
    {
        public static readonly string Name = "SampleItem";

        public SampleItem()
            : base(Name)
        {
            SimpleIntValue = new SimpleValue<int>("IntValue", this);
        }

        public SimpleValue<int> SimpleIntValue { get; private set; }
    }
}
