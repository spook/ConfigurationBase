﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationBase.Tests.SampleConfigurations
{
    public class SampleItemCollection : SimpleCollection<SampleItem>
    {
        public SampleItemCollection(string xmlName, BaseItemContainer parent)
            : base(xmlName, parent, SampleItem.Name)
        {
            
        }
    }
}
