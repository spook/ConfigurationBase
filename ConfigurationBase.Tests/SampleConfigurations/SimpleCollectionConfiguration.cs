﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationBase.Tests.SampleConfigurations
{
    public class SimpleCollectionConfiguration : BaseConfiguration
    {
        public static readonly string XmlNodeName = "SimpleCollectionConfiguration";

        public SimpleCollectionConfiguration()
            : base(XmlNodeName)
        {
            Collection = new SampleItemCollection("Collection", this);
        }

        public SampleItemCollection Collection { get; private set; }
    }
}
