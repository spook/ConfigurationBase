﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationBase.Tests.SampleConfigurations
{
    public class SimpleValuesConfiguration : BaseConfiguration
    {
        public SimpleValuesConfiguration()
            : base("Simple")
        {
            StringValue = new SimpleValue<string>("StringValue", this);
            IntValue = new SimpleValue<int>("IntValue", this);
            EnumValue = new SimpleValue<SimpleEnum>("EnumValue", this);
        }

        public SimpleValue<SimpleEnum> EnumValue { get; private set; }
        public SimpleValue<int> IntValue { get; private set; }
        public SimpleValue<string> StringValue { get; private set; }
    }
}
