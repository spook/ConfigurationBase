﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConfigurationBase.Tests.SampleConfigurations;
using System.IO;

namespace ConfigurationBase.Tests
{
    [TestClass]
    public class StorageTests
    {
        [TestMethod]
        public void SaveLoadTest1()
        {
            // Arrange

            SimpleValuesConfiguration configuration = new SimpleValuesConfiguration();
            configuration.IntValue.Value = 4;
            configuration.StringValue.Value = "Ala ma kota";
            configuration.EnumValue.Value = SimpleEnum.Seven;

            // Act

            MemoryStream ms = new MemoryStream();
            configuration.Save(ms);
            ms.Seek(0, SeekOrigin.Begin);

            SimpleValuesConfiguration configuration2 = new SimpleValuesConfiguration();
            configuration2.Load(ms);

            // Assert

            Assert.AreEqual(4, configuration2.IntValue.Value);
            Assert.AreEqual("Ala ma kota", configuration2.StringValue.Value);
            Assert.AreEqual(SimpleEnum.Seven, configuration2.EnumValue.Value);
        }

        [TestMethod]
        public void SaveLoadTest2()
        {
            // Arrange

            SimpleCollectionConfiguration config = new SimpleCollectionConfiguration();

            for (int i = 0; i < 3; i++)
            {
                SampleItem item = new SampleItem();
                item.SimpleIntValue.Value = i;
                config.Collection.Add(item);
            }

            // Act

            MemoryStream ms = new MemoryStream();
            config.Save(ms);
            ms.Seek(0, SeekOrigin.Begin);

            SimpleCollectionConfiguration config2 = new SimpleCollectionConfiguration();
            config2.Load(ms);

            // Assert

            Assert.AreEqual(3, config2.Collection.Count);
            for (int i = 0; i < config2.Collection.Count; i++)
                Assert.AreEqual(i, config2.Collection[i].SimpleIntValue.Value);            
        }
    }
}
