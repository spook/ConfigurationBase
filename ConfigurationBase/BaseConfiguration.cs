﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ConfigurationBase
{
    public class BaseConfiguration : BaseItemContainer
    {
        // Private fields -----------------------------------------------------

        private bool dirty;
        private bool notificationsSuspended;
        private bool isLoading;

        // Internal methods ---------------------------------------------------

        internal override void NotifyChanged()
        {
            base.NotifyChanged();
            
            if (isLoading)
            {
                // Ignore change notifications during loading
                return;
            }
            if (notificationsSuspended)
            {
                dirty = true;
            }
            else
            {
                dirty = false;
                OnConfigurationChanged();
            }
        }

        // Protected methods --------------------------------------------------

        protected void OnConfigurationChanged()
        {
            ConfigurationChanged?.Invoke(this, EventArgs.Empty);
        }

        // Public methods -----------------------------------------------------

        public BaseConfiguration(string XmlName)
            : base(XmlName)
        {
            dirty = false;
            notificationsSuspended = false;
            isLoading = false;
        }

        public void Load(string filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                Load(fs);
            }
        }

        public void Load(Stream stream)
        {
            isLoading = true;

            try
            {
                XmlDocument document;

                try
                {
                    document = new XmlDocument();
                    document.Load(stream);
                }
                catch (Exception e)
                {
                    throw new FileLoadException("Cannot load configuration file!", e);
                }

                XmlNode root = document[XmlName];
                if (root != null)
                {
                    InternalLoad(root);
                }
                else
                {
                    throw new FileLoadException("Invalid configuration file format!");
                }
            }
            finally
            {
                isLoading = false;
            }
        }

        public void Save(string filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                Save(fs);
            }
        }

        public void Save(Stream stream)
        {
            XmlDocument document = new XmlDocument();

            XmlElement root = document.CreateElement(XmlName);
            document.AppendChild(root);

            InternalSave(root);

            StreamWriter sw = new StreamWriter(stream);
            XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            });
            document.Save(writer);            
        }

        public void SuspendNotifications()
        {
            notificationsSuspended = true;
        }

        public void ResumeNotifications(bool sendPending = true)
        {
            if (dirty)
            {
                if (sendPending)
                    OnConfigurationChanged();
                dirty = false;
            }

            notificationsSuspended = false;
        }

        // Public properties --------------------------------------------------

        public event EventHandler ConfigurationChanged;      
    }
}
