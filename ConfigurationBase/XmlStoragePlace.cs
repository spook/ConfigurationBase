﻿namespace ConfigurationBase
{
    public enum XmlStoragePlace
    {
        Subnode,
        Attribute
    }
}